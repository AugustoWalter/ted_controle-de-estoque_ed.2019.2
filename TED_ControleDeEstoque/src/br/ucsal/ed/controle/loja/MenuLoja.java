package br.ucsal.ed.controle.loja;

import javax.swing.JOptionPane;

import br.ucsal.ed.controle.estoque.ControleEstoque;
import br.ucsal.ed.controle.estoque.MenuEstoque;
import br.ucsal.ed.controle.venda.ControleVenda;
import br.ucsal.ed.controle.venda.ListaVenda;

public class MenuLoja {

	public static void main(String[] args) {
		executar(new ControleVenda());
	}

	private static void executar(ControleVenda venda) {

		ControleEstoque lem = new ControleEstoque();
		lem.cadastraCategoria(1, "Alimentos");
		lem.cadastraCategoria(2, "Produtos de Limpeza");
		lem.cadastraCategoria(3, "Papelaria");

		lem.cadastraProduto(1, 1, "Feij�o", 5.50, 220, "05/10/2005");
		lem.cadastraProduto(1, 2, "Arroz", 20.0, 300, "00/00/00");
		lem.cadastraProduto(1, 3, "Carne", 2.0, 50, "00/00/00");
		lem.cadastraProduto(2, 1, "Detergente", 0.80, 500, "00/00/00");
		lem.cadastraProduto(2, 2, "Sab�o", 5.0, 200, "00/00/00");
		lem.cadastraProduto(3, 1, "Lapis", 0.50, 450, "00/00/00");
		lem.cadastraProduto(3, 2, "Borracha", 0.10, 30, "00/00/00");
		lem.cadastraProduto(3, 3, "Caneta", 1.50, 600, "00/00/00");

		int dec;

		do {
			dec = Integer.parseInt(JOptionPane.showInputDialog(null,
					":<<Menu Venda>>: \n| 1 - Vender | 2 - Cadastrar Item \n| 3 - Cancelar Item"
							+ "| 4 - Consultar Venda \n| 5 - Imprimir Nota Fiscal " + "| 6 - Menu do Estoque\n"
							+ "| 7 - Sair\n" + ""));

			switch (dec) {

			case 1:
				venda.vender();

				break;
			case 2:
				int codCat = Integer.parseInt(JOptionPane.showInputDialog(null, "insira o codigo de categoria: "));
				int codProduto = Integer.parseInt(JOptionPane.showInputDialog(null, "insira o codigo do produto: "));
				int quantidade = Integer.parseInt(JOptionPane.showInputDialog(null, "insira a quantidade: "));
				venda.cadastraItemVenda(codCat, codProduto, quantidade);

				break;
			case 3:
				String nomeProduto = JOptionPane.showInputDialog(null, "insira o nome da categoria: ");
				venda.cancelarItemVenda(nomeProduto);
				break;
			case 4:

				int numero = Integer.parseInt(
						JOptionPane.showInputDialog(null, "insira o codigo da categoria que dejesa" + " consultar: "));
				ListaVenda v = venda.cunsultarVenda(numero);
				if (v != null) {
					JOptionPane.showMessageDialog(null, v.getValorTotal());
				}

				break;
			case 5:
				int notaFiscal = Integer.parseInt(JOptionPane.showInputDialog(null, "insira o n�mero: "));
				venda.imprimeNotaFiscal(notaFiscal);

				break;
			case 6:
				MenuEstoque.executaMenu(lem);

				break;
			case 7:
				String msg = "Programa Encerrado!";
				JOptionPane.showMessageDialog(null, msg);

				break;
			default:

				String msg2 = "Op��o Inv�lida! Tente novamente.";
				JOptionPane.showMessageDialog(null, msg2);

			}

		} while (dec < 7);

	}

}
