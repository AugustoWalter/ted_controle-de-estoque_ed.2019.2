package br.ucsal.ed.controle.estoque;

import javax.swing.JOptionPane;

import br.ucsal.ed.controle.loja.MenuLoja;

public class MenuEstoque {
	
//	public static void main(String[] args) {
//		
//		executaMenu(new ControleEstoque());
//		
//	}

	public static void executaMenu(ControleEstoque controle) {
		
		int dec;

		do {
			dec = Integer.parseInt(JOptionPane.showInputDialog(null,
					":<<Menu Agenda>>: \n| 1 - Cadastrar Categoria | 2 - Remover Categoria \n| 3 - Alterar Categoria "
							+ "| 4 - Consultar Categoria \n| 5 - Cadastrar Produto "
							+ "| 6 - Remover Produto \n| 7 - Consultar Produto | 8 - Imprimir Estoque \n"
							+ "| 9 - Imprimir Categoria | 10 - Imprimir Dados das Categoria \n| 11 - Voltar\n" + "")); 

			switch (dec) {

			case 1:
				int codigo = Integer.parseInt(JOptionPane.showInputDialog(null, "insira o codigo de categoria: "));
				String descricao = JOptionPane.showInputDialog(null, "insira o nome da categoria: ");
				controle.cadastraCategoria(codigo, descricao);

				break;
			case 2:

				codigo = Integer.parseInt(
						JOptionPane.showInputDialog(null, "insira o codigo da categoria que dejesa" + " remover: "));
				controle.removeCategoria(codigo);

				break;
			case 3:
				codigo = Integer.parseInt(
						JOptionPane.showInputDialog(null, "insira o codigo da categoria que dejesa" + " alterar: "));
				descricao = JOptionPane.showInputDialog(null, "informe o novo nome dessa categoria");
				controle.alteraCategoria(codigo, descricao);

				break;
			case 4:
				codigo = Integer.parseInt(
						JOptionPane.showInputDialog(null, "insira o codigo da categoria que dejesa" + " consultar: "));
				Categoria cat = controle.consultaCategoria(codigo);
				if (cat != null) {
					JOptionPane.showMessageDialog(null, cat.getDescricao());
				}

				break;
			case 5:
				codigo = Integer.parseInt(JOptionPane.showInputDialog(null,
						"insira o codigo da categoria que dejesa" + " adicionar um produto: "));
				int codigoProduto = Integer
						.parseInt(JOptionPane.showInputDialog(null, "insira o codigo do novo produto:"));
				String descricaoProduto = JOptionPane.showInputDialog(null, "insira o nome do novo produto:");
				double valor = Double.parseDouble(JOptionPane.showInputDialog(null, "insira o pre�o do produto:"));
				int quantidade = Integer
						.parseInt(JOptionPane.showInputDialog(null, "informe a quantidade do produto em estoque:"));
				String dataVencimento = JOptionPane.showInputDialog(null, "informe a data de vencimento do produto:");
				controle.cadastraProduto(codigo, codigoProduto, descricaoProduto, valor, quantidade, dataVencimento);

				break;
			case 6:
				codigo = Integer.parseInt(JOptionPane.showInputDialog(null,
						"insira o codigo da categoria que dejesa" + " remover um produto: "));
				codigoProduto = Integer
						.parseInt(JOptionPane.showInputDialog(null, "insira o codigo produto a ser removido:"));
				controle.removeProduto(codigo, codigoProduto);

				break;
			case 7:

				codigo = Integer.parseInt(JOptionPane.showInputDialog(null,
						"informe o codigo da categoria que dejesa" + " consultar um produto: "));
				if (controle.consultaCategoria(codigo) != null) {

					codigoProduto = Integer
							.parseInt(JOptionPane.showInputDialog(null, "insira o codigo do produto desejado:"));
					Produto prot = controle.consultaProduto(codigo, codigoProduto);

					if (prot != null) {
						JOptionPane.showMessageDialog(null, prot.getDescricao());
					}
				}
				break;
			case 8:
				controle.imprimeEstoque();

				break;
			case 9:
				codigo = Integer.parseInt(JOptionPane.showInputDialog(null,
						"informe o codigo da categoria que dejesa" + " vizualizar: "));
				controle.imprimeCategoria(codigo);

				break;
			case 10:
				controle.imprimeDadosCategoria();

				break;

			case 11:
				MenuLoja.main(null);
				
				break;

			default:

				String msg2 = "Op��o Inv�lida! Tente novamente.";
				JOptionPane.showMessageDialog(null, msg2);

			}

		} while (dec != 11);

	}

}