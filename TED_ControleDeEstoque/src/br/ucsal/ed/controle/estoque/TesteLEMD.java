package br.ucsal.ed.controle.estoque;

public class TesteLEMD {

	public static void main(String[] args) {

		executa(new ControleEstoque());

	}

	private static void executa(ICE lem) {

		lem.cadastraCategoria(1, "Alimentos");
		lem.cadastraCategoria(2, "Produtos de Limpeza");
		lem.cadastraCategoria(3, "Papelaria");

		lem.cadastraProduto(1, 1, "Feij�o", 5.50, 220, "05/10/2005");
		lem.cadastraProduto(1, 2, "Arroz", 20.0, 300, "00/00/00");
		lem.cadastraProduto(1, 3, "Carne", 2.0, 50, "00/00/00");
		lem.cadastraProduto(2, 1, "Detergente", 0.80, 500, "00/00/00");
		lem.cadastraProduto(2, 2, "Sab�o", 5.0, 200, "00/00/00");
		lem.cadastraProduto(3, 1, "Lapis", 0.50, 450, "00/00/00");
		lem.cadastraProduto(3, 2, "Borracha", 0.10, 30, "00/00/00");
		lem.cadastraProduto(3, 3, "Caneta", 1.50, 600, "00/00/00");
//		lem.imprimeCategoria(3);
//		lem.removeCategoria(4);
//		lem.removeCategoria(5);
//		lem.removeProduto(2, 3);
//		lem.removeProduto(4, 1);
//		lem.removeProduto(1, 5);

//		lem.imprimeEstoque();
		lem.imprimeDadosCategoria();

	}
}