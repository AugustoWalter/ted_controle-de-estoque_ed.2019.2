package br.ucsal.ed.controle.estoque;

public interface ICE {

	/*
	 * Cadastra uma categoria nova.
	 */
	void cadastraCategoria(int codigo, String descricao);

	/*
	 * Remove uma categoria da lista.
	 */
	void removeCategoria(int codigo);

	/*
	 * Altera a descri��o de uma categoria.
	 */
	void alteraCategoria(int codigo, String descricao);

	/*
	 * Mostra as informa��es de uma categoria.
	 */
	Categoria consultaCategoria(int codigo);

	/*
	 * Cadastra um produto novo em uma categoria existente.
	 */
	void cadastraProduto(int codigo, int codigoProduto, String descricao, double valor, int quantidade,
			String dataVencimento);

	/*
	 * Remove um produto da lista de sua categoria.
	 */
	void removeProduto(int codigo, int codigoProduto);

	/*
	 * Mostra as informa��es de um produto.
	 */
	Produto consultaProduto(int codigo, int codigoProduto);

	/*
	 * Mostra a descri��o de todas as categoria e a descri��o de seus repectivos
	 * produtos.
	 */
	void imprimeEstoque();

	/*
	 * Mostra a descri��o de uma categoria e a descri��o de seus repectivos
	 * produtos.
	 */
	void imprimeCategoria(int codigo);

	/*
	 * Mostra todos os dados das categorias e de seus produtos.
	 */
	void imprimeDadosCategoria();
	
	/*
	 * Calcula a media ponderada dos produtos de mesma descri��o.
	 */
	Double valorMedio(int codigo, int codigoProduto, String descricao);

}
