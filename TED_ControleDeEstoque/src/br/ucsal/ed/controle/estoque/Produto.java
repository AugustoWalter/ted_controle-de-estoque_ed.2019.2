package br.ucsal.ed.controle.estoque;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Produto {

	private Random rand = new Random();
	private int b1;
	private int b2;
	private int b3;
	private int b4;
	private int codigoProduto;
	private String descricao;
	private double valor;
	private int quantidade;
	private Date dataVencimento;
	public String codigoBarra;
	private Produto prox;

	private SimpleDateFormat formatdata = new SimpleDateFormat("dd/MM/yyyy");

	public Produto(int codigoProduto, String descricao, double valor, int quantidade, String dataVencimento) {
		this.codigoProduto = codigoProduto;
		this.descricao = descricao;
		this.valor = valor;
		this.quantidade = quantidade;

		try {
			this.dataVencimento = formatdata.parse(dataVencimento);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		b1 = rand.nextInt(9000) + 1000;
		b2 = rand.nextInt(9000) + 1000;
		b3 = rand.nextInt(9000) + 1000;
		b4 = rand.nextInt(9) + 1;

	}

	public int getCodigoProduto() {
		return codigoProduto;
	}

	public void setCodigoProduto(int codigoProduto) {
		this.codigoProduto = codigoProduto;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public String getdataVencimento() {

		String dta = formatdata.format(dataVencimento);

		return dta;
	}

	public void setdataVencimento(String dataVencimento) {
		try {
			this.dataVencimento = formatdata.parse(dataVencimento);
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

	public String getCodigoBarra() {
		return codigoBarra = b1 + " " + b2 + " " + b3 + " " + b4;

	}

	public Produto getProx() {
		return prox;
	}

	public void setProx(Produto prox) {
		this.prox = prox;
	}

}
