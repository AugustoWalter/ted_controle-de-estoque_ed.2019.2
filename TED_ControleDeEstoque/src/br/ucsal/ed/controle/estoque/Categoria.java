package br.ucsal.ed.controle.estoque;

public class Categoria {

	private int codigoCategoria;
	private String descricao;
	private Categoria prox;
	private Produto inicio = null;

	public Categoria(int codigoCategoria, String descricao) {
		this.codigoCategoria = codigoCategoria;
		this.descricao = descricao;
	}

	public int getCodigoCategoria() {
		return codigoCategoria;
	}

	public void setCodigoCategoria(int codigoCategoria) {
		this.codigoCategoria = codigoCategoria;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Categoria getProx() {
		return prox;
	}

	public void setProx(Categoria prox) {
		this.prox = prox;
	}

	public Produto getInicio() {
		return inicio;
	}

	public void setInicio(Produto inicio) {
		this.inicio = inicio;
	}

	public boolean ehVazia() {
		return inicio == null;
	}

}
