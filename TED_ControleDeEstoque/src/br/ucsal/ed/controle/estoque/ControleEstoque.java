package br.ucsal.ed.controle.estoque;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JOptionPane;

public class ControleEstoque implements ICE {

	public static Categoria inicio = null;
	public static int tamanho = 0;

	public void cadastraCategoria(int codigo, String descricao) {

		Categoria novo = new Categoria(codigo, descricao);
		novo.setCodigoCategoria(codigo);
		novo.setDescricao(descricao);
		if (testVazia()) {
			inicio = novo;

		} else {
			novo.setProx(inicio);
			inicio = novo;
		}
		tamanho++;

		File file = new File("Categoria.txt");
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));
				BufferedReader br = new BufferedReader(new FileReader(file))) {
			bw.write("Código Categoria:" + codigo + " ; " + "Descrição:" + descricao);
			bw.newLine();
			bw.flush();

			Object s;
			while ((s = br.readLine()) != null) {
				System.out.println(s);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void removeCategoria(int codigo) {

		if (testVazia()) {
			JOptionPane.showMessageDialog(null, "Não existem categorias na lista!");

		} else {
			Categoria ant = null;
			Categoria aux = inicio;
			while (aux.getProx() != null && aux.getCodigoCategoria() != codigo) {
				ant = aux;
				aux = aux.getProx();
			}
			if (aux != null && aux.getCodigoCategoria() == codigo) {
				if (ant == null) {
					inicio = inicio.getProx();
					aux = null;
				} else if (aux.getProx() == null) {
					if (aux.getCodigoCategoria() == codigo) {
						ant.setProx(null);
						aux = null;
					}
				} else {
					ant.setProx(aux.getProx());
					aux = null;
				}
				tamanho--;
			} else {
				JOptionPane.showMessageDialog(null, "Categoria solicitada não contem na lista!");
			}
		}

		File file = new File("Categoria.txt");
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));
				BufferedReader br = new BufferedReader(new FileReader(file))) {
			bw.write("Código Categoria:" + codigo);
			bw.flush();

			Object s;
			while ((s = br.readLine()) != null) {
				System.out.println(s);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void alteraCategoria(int codigo, String descricao) {

		if (testVazia()) {
			JOptionPane.showMessageDialog(null, "Não existem categorias na lista!");

		} else {
			Categoria aux = inicio;
			while (aux.getProx() != null && aux.getCodigoCategoria() != codigo) {
				aux = aux.getProx();
			}
			if (aux != null && aux.getCodigoCategoria() == codigo) {
				aux.setDescricao(descricao);
			} else {
				JOptionPane.showMessageDialog(null, "Categoria solicitada não contem na lista!");

			}
		}

	}

	public Categoria consultaCategoria(int codigo) {
		Categoria aux = inicio;
		if (testVazia()) {
			JOptionPane.showMessageDialog(null, "Não existem categorias na lista!");
		} else {
			while (aux.getProx() != null && aux.getCodigoCategoria() != codigo) {
				aux = aux.getProx();
			}
			if (aux != null && aux.getCodigoCategoria() == codigo) {
				return aux;
			} else {
				JOptionPane.showMessageDialog(null, "Categoria solicitada não contem na lista!");

			}
		}

		return null;
	}

	public void cadastraProduto(int codigo, int codigoProduto, String descricao, double valor, int quantidade,
			String dataVencimento) {

		Produto novo = new Produto(codigoProduto, descricao, valor, quantidade, dataVencimento);
		novo.setCodigoProduto(codigoProduto);
		novo.setDescricao(descricao);
		novo.setValor(valor);
		novo.setQuantidade(quantidade);
		novo.setdataVencimento(dataVencimento);
		if (testVazia()) {
			JOptionPane.showMessageDialog(null, "Não existem categorias na lista!");

		} else {
			Categoria auxC = inicio;
			while (auxC.getProx() != null && auxC.getCodigoCategoria() != codigo) {
				auxC = auxC.getProx();
			}
			if (auxC != null && auxC.getCodigoCategoria() == codigo) {
				if (auxC.ehVazia()) {
					auxC.setInicio(novo);
				} else {
					novo.setProx(auxC.getInicio());
					auxC.setInicio(novo);
				}
			} else {
				JOptionPane.showMessageDialog(null, "Categoria solicitada não contem na lista!");
			}
		}

		File file = new File("Poduto.txt");
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));
				BufferedReader br = new BufferedReader(new FileReader(file))) {
			bw.write("Código Categoria:" + codigo + " ; " + "Código Produto:" + codigoProduto + " ; " + "Descrição:"
					+ descricao + " ; " + "Valor:" + valor + " ; " + "Quantidade:" + quantidade + " ; "
					+ "Data de vencimento:" + dataVencimento);
			bw.newLine();
			bw.flush();

			Object s;
			while ((s = br.readLine()) != null) {
				System.out.println(s);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void removeProduto(int codigo, int codigoProduto) {

		if (testVazia()) {
			JOptionPane.showMessageDialog(null, "Não existem categorias na lista!");

		} else {
			Categoria auxC = inicio;
			while (auxC.getProx() != null && auxC.getCodigoCategoria() != codigo) {
				auxC = auxC.getProx();
			}
			if (auxC != null && auxC.getCodigoCategoria() == codigo) {
				if (auxC.ehVazia()) {
					JOptionPane.showMessageDialog(null, "Não possui produtos cadastrados nesta categoria!");
				} else {
					Produto ant = null;
					Produto auxP = auxC.getInicio();
					while (auxP.getProx() != null && auxP.getCodigoProduto() != codigoProduto) {
						ant = auxP;
						auxP = auxP.getProx();
					}
					if (auxP.getCodigoProduto() == codigoProduto) {
						if (ant == null) {
							auxC.setInicio(auxP.getProx());
							auxP = null;
						} else if (auxP.getProx() == null) {
							if (auxP.getCodigoProduto() == codigoProduto) {
								ant.setProx(null);
								auxP = null;
							}
						} else {
							ant.setProx(auxP.getProx());
							auxP = null;
						}
					} else {
						JOptionPane.showMessageDialog(null, "Produto não encontrado nessa categoria!");
					}
				}
			} else {
				JOptionPane.showMessageDialog(null, "Categoria solicitada não contem na lista!");

			}

			File file = new File("Produto.txt");
			try (BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));
					BufferedReader br = new BufferedReader(new FileReader(file))) {
				bw.write("Código Categoria:" + codigo + " ; " + "Código Produto:" + codigoProduto);
				bw.flush();

				Object s;
				while ((s = br.readLine()) != null) {
					System.out.println(s);
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	public Produto consultaProduto(int codigo, int codigoProduto) {

		if (testVazia()) {
			JOptionPane.showMessageDialog(null, "Não existem categorias na lista!");

		} else {
			Categoria cat = consultaCategoria(codigo);

			if (cat == null) {
				JOptionPane.showMessageDialog(null, "Categoria solicitada não contem na lista!");

			} else {
				Produto aux = cat.getInicio();
				while (aux.getProx() != null && aux.getCodigoProduto() != codigoProduto) {
					aux = aux.getProx();
				}
				if (aux != null && aux.getCodigoProduto() == codigoProduto) {
					return aux;
				} else {
					JOptionPane.showMessageDialog(null, "Produto não encontrado nessa categoria!");

				}
			}
		}

		return null;
	}

	public void imprimeEstoque() {

		String mensagem = "";

		if (testVazia()) {
			mensagem = "Não existem categorias na lista!";
		} else {
			Categoria auxC = inicio;
			while (auxC != null) {
				mensagem += auxC.getDescricao() + ": ";

				if (auxC.ehVazia()) {
					mensagem += "Não há Produtos nesta categoria...";
				} else {
					Produto auxP = auxC.getInicio();
					while (auxP != null) {
						if (auxP.getProx() == null) {
							mensagem += auxP.getDescricao() + "\n";
						} else {
							mensagem += auxP.getDescricao() + ", ";
						}
						auxP = auxP.getProx();
					}
				}
				auxC = auxC.getProx();
			}
		}
		JOptionPane.showMessageDialog(null, mensagem);
	}

	public void imprimeCategoria(int codigo) {

		String mensagem = "";

		if (testVazia()) {
			mensagem = "Não existem categorias na lista!";
		} else {
			Categoria auxC = inicio;
			while (auxC.getProx() != null && auxC.getCodigoCategoria() != codigo) {
				auxC = auxC.getProx();
			}
			if (auxC.getProx() == null && auxC.getCodigoCategoria() != codigo) {
				mensagem += "Categoria não encontrada...";
			} else {
				mensagem = auxC.getDescricao() + ": ";
				if (auxC.ehVazia()) {
					mensagem += "Não há Produtos nesta categoria...";
				} else {
					Produto auxP = auxC.getInicio();
					while (auxP != null) {
						if (auxP.getProx() == null) {
							mensagem += auxP.getDescricao();
						} else {
							mensagem += auxP.getDescricao() + ", ";
						}
						auxP = auxP.getProx();
					}
				}
			}

		}
		JOptionPane.showMessageDialog(null, mensagem);
	}

	public void imprimeDadosCategoria() {

		String mensagem = "";

		if (testVazia()) {
			mensagem = "Não existem categorias na lista!";
		} else {
			Categoria auxC = inicio;
			while (auxC != null) {
				mensagem += "(" + auxC.getCodigoCategoria() + ")" + auxC.getDescricao() + ":\n";

				if (auxC.ehVazia()) {
					mensagem += "Não há Produtos nesta categoria...";
				} else {
					Produto auxP = auxC.getInicio();
					while (auxP != null) {
						mensagem += "(" + auxP.getCodigoProduto() + ")" + auxP.getDescricao() + ": \n"
								+ auxP.getQuantidade() + " unidade(s) - R$ " + auxP.getValor() + "\n" + "vencimento "
								+ auxP.getdataVencimento() + " - codigo de barras " + auxP.getCodigoBarra() + "\n";

						auxP = auxP.getProx();
					}
				}
				auxC = auxC.getProx();
			}
		}
		JOptionPane.showMessageDialog(null, mensagem);

	}

	private boolean testVazia() {
		return inicio == null;
	}

	public Double valorMedio(int codigo, int codigoProduto, String descricao) {

		Double media = 0.0;
		int qntTotal = 0;

		Produto achado = consultaProduto(codigo, codigoProduto);
		while (achado.getProx() != null) {
			if (achado.getDescricao() == descricao) {
				media += achado.getQuantidade() * achado.getValor();
				qntTotal += achado.getQuantidade();
			}
		}
		media /= qntTotal;

		return media;
	}

}
