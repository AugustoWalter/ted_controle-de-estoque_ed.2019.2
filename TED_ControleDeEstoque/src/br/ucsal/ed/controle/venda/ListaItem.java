package br.ucsal.ed.controle.venda;

public class ListaItem {

	private Double valor;
	private String produto;
	private int qnt;
	private ListaItem prox;

	public ListaItem(String produto, Double valor, int qnt) {
		this.valor = valor;
		this.produto = produto;
		this.qnt = qnt;
	}

	public ListaItem getProx() {
		return prox;
	}

	public void setProx(ListaItem prox) {
		this.prox = prox;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public String getProduto() {
		return produto;
	}

	public void setProduto(String produto) {
		this.produto = produto;
	}

	public int getQnt() {
		return qnt;
	}

	public void setQnt(int qnt) {
		this.qnt = qnt;
	}

	@Override
	public String toString() {
		return "produto = " + getProduto() + ", valor = " + getValor() + ", qnt = " + getQnt();
	}

}
