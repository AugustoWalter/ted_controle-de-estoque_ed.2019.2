package br.ucsal.ed.controle.venda;

import javax.swing.JOptionPane;

import br.ucsal.ed.controle.estoque.*;

public class ControleVenda implements IV {

	private ListaVenda inicioV = null;

	public ListaVenda iniciarVenda() {

		ListaVenda venda = new ListaVenda(0.0, "");
		venda.setValorTotal(0.0);
		venda.setNotaFiscal("");

		if (inicioV == null) {
			inicioV = venda;
		} else {
			venda.setProx(inicioV);
			inicioV = venda;
		}

		return venda;
	}

	public void cadastraItemVenda(int codCat, int codProd, int qnt) {

		ControleEstoque controle = new ControleEstoque();
		Produto vendido = controle.consultaProduto(codCat, codProd);
		ListaItem itens = new ListaItem(vendido.getDescricao(), vendido.getValor(), qnt);
		itens.setProduto(vendido.getDescricao());
		itens.setValor(vendido.getValor());
		itens.setQnt(qnt);
		if (inicioV == null) {
			JOptionPane.showMessageDialog(null, "N�o foi iniciado uma venda");
		} else {
			if (inicioV.getInicio() == null) {
				inicioV.setInicio(itens);
			} else {
				itens.setProx(inicioV.getInicio());
				inicioV.setInicio(itens);
			}
		}
	}

	public void vender() {

		if(inicioV == null) {
			iniciarVenda();
		}else {
			if (inicioV.getInicio() == null) {
				JOptionPane.showMessageDialog(null,
						"N�o existem itens a serem vendidos porfavor adicionar todos os itens antes de efetuar venda");
	
			} else {
				ListaItem aux = inicioV.getInicio();
				int resp = 0;
				Double valorTotal = 0.0;
				String notaFiscal = "";
				inicioV.setInicio(inicioV.getInicio());
	
				do {
					valorTotal += aux.getValor() * aux.getQnt();
					aux = aux.getProx();
				} while (aux != null);
	
				aux = inicioV.getInicio();
	
				do {
					notaFiscal += "\n" + aux.toString();
					aux = aux.getProx();
				} while (aux != null);
	
				inicioV.setNotaFiscal(notaFiscal);
				inicioV.setValorTotal(valorTotal);
				
				resp = Integer.parseInt(JOptionPane.showInputDialog(null, "Deseja salvar a venda? 1-Sim 2-N�o"));
				cancelarVenda(resp);
			}
		}
		
	}

	public void cancelarVenda(int resp) {
		ListaItem aux = inicioV.getInicio();
		Categoria auxC = ControleEstoque.inicio;

		if (resp == 2) {
			if (inicioV.getProx() == null) {
				inicioV = null;
			} else {
				inicioV = inicioV.getProx();
			}
		} else if (resp == 1) {
			do {
				Produto auxP = auxC.getInicio();
				while (auxP.getProx() != null && auxP.getDescricao() != aux.getProduto()) {
					auxP = auxP.getProx();
				}
				int quantidade = auxP.getQuantidade() - aux.getQnt();
				auxP.setQuantidade(quantidade);
				auxC = auxC.getProx();
			} while (auxC.getProx() != null);
		}

	}

	public ListaVenda cunsultarVenda(int numero) {

		ListaVenda aux = inicioV;

		if (inicioV == null) {
			JOptionPane.showMessageDialog(null, "N�o foi feita nenhuma venda");
		} else {
			while (aux.getProx() != null && aux.getNumero() != numero) {
				aux = aux.getProx();
			}
			if (aux != null && aux.getNumero() == numero) {
				return aux;
			} else {
				JOptionPane.showMessageDialog(null, "Categoria solicitada n�o contem na lista!");

			}
		}

		return null;
	}

	public void cancelarItemVenda(String nomeProd) {

		ListaItem ant = null;
		ListaItem aux = inicioV.getInicio();
		ListaVenda auxV = inicioV;

		if (inicioV.getInicio() == null) {
			JOptionPane.showMessageDialog(null, "N�o foi feita nenhuma venda");
		} else {

			while (aux.getProx() != null && aux.getProduto() != nomeProd) {
				ant = aux;
				aux = aux.getProx();
			}
			if (aux != null && aux.getProduto() == nomeProd) {
				if (ant == null) {
					auxV.setInicio(inicioV.getInicio().getProx());
					aux = null;
				} else if (aux.getProx() == null) {
					ant.setProx(null);
					aux = null;
				} else {
					ant.setProx(aux.getProx());
					aux = null;
				}
			}
		}

	}

	public void imprimeNotaFiscal(int numero) {

		ListaVenda aux = iniciarVenda();

		while (aux.getProx() != null && aux.getNumero() != numero) {
			aux = aux.getProx();
		}
		if (aux.getNumero() == numero) {
			JOptionPane.showMessageDialog(null, "Numero da venda " + aux.getNumero() + "\n" + aux.getNotaFiscal()
					+ "\n Total = " + aux.getValorTotal());
		}
	}

}
