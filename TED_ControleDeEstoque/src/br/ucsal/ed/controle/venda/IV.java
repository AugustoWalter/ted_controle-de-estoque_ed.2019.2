package br.ucsal.ed.controle.venda;


public interface IV {
	
	void vender();
	void cancelarVenda(int resp);
	ListaVenda cunsultarVenda(int numero);
	void cadastraItemVenda(int codCat, int codProd, int qnt);
	void cancelarItemVenda(String nomeProd);
	void imprimeNotaFiscal(int numero);
	
	
}
