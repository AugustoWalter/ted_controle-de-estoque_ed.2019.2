package br.ucsal.ed.controle.venda;

public class ListaVenda {

	private static int NUM = 0;

	private int numero;
	private Double valorTotal;
	private String notaFiscal;
	private ListaVenda prox;
	private ListaItem inicio;
	
	public ListaVenda(Double valorTotal, String notaFiscal) {
		NUM++;
		this.numero = NUM;
		this.valorTotal = valorTotal;
		this.notaFiscal = notaFiscal;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public String getNotaFiscal() {
		return notaFiscal;
	}

	public void setNotaFiscal(String notaFiscal) {
		this.notaFiscal = notaFiscal;
	}

	public ListaVenda getProx() {
		return prox;
	}

	public void setProx(ListaVenda prox) {
		this.prox = prox;
	}

	public ListaItem getInicio() {
		return inicio;
	}

	public void setInicio(ListaItem inicio) {
		this.inicio = inicio;
	}

	public int getNumero() {
		return numero;
	}
	
	public void setNumero() {
		this.numero = NUM;
	}

}
